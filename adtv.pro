#-------------------------------------------------
#
# Project created by QtCreator 2016-07-06T20:25:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = adtv
TEMPLATE = app


SOURCES += main.cpp\
        adtv.cpp

HEADERS  += adtv.h

FORMS    += adtv.ui
