#ifndef ADTV_H
#define ADTV_H

#include <QMainWindow>
#include <QProcess>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QFont>
#include <QDesktopWidget>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/time.h>

#include <time.h>
#include <unistd.h>

#include <linux/dvb/frontend.h>
#include <linux/dvb/dmx.h>

namespace Ui {
class adtv;
}


class adtv : public QMainWindow
{
    Q_OBJECT





public:
    int ota_enabled_flag;


    int wide;
    int high;
    int posy=20;
    int posx=20;

    int fd0;
    int fd1;
    int fd2;
    int fd3;

    int fetype;
    int streamFlag;
    fe_status_t status;

    QString fename;
    QString stuff;
    QString scan_adapter = "-a ";

    struct dvb_frontend_info fe_stuff0;
    struct dvb_frontend_info fe_stuff1;
    struct dvb_frontend_info fe_stuff2;
    struct dvb_frontend_info fe_stuff3;

    const char *frontend_name0;
    const char *frontend_name1;
    const char *frontend_name2;
    const char *frontend_name3;

    explicit adtv(QWidget *parent = 0);
    ~adtv();


private slots:
    void on_channelBspinBox_valueChanged(int channel);

    void on_channelAspinBox_valueChanged(int channel);

    void on_channelCspinBox_valueChanged(int channel);

    void on_channelDspinBox_valueChanged(int channel);

    void on_pushButton_clicked();

    void ShowVideos();

    void createStreams();

    void ShowStreams();

    void TuneTo();

    void ScanTuner();

    void readScan();

    void stopTunerA();

    void reconfCreate();

    void loadReconf();

  //  void ScanDVBtuner();

//    void readDvbScan();

//    void stopDvbA();

    void dvbreconfCreate();

    void loadDvbReconf();

    void on_tuneTo_radioButt_clicked();

    void cnvfrqtchnl(QString temp);

    void on_showStreamradioButt_clicked();

    bool channelGood(int value);

    bool transponderGood(int value);

    void check_adapter_FE();

    void open_fd(int cnt);

    void on_createStreamsradioButt_toggled(bool checked);

    void on_rtpButton_toggled(bool checked);

    void showHotkeys();

private:
    Ui::adtv *ui;
};

#endif // ADTV_H
