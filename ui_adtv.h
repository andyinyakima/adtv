/********************************************************************************
** Form generated from reading UI file 'adtv.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADTV_H
#define UI_ADTV_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_adtv
{
public:
    QWidget *centralWidget;
    QGroupBox *FunctiongroupBox;
    QRadioButton *tuneTo_radioButt;
    QRadioButton *ATSCscanradioButt;
    QRadioButton *showStreamradioButt;
    QPushButton *pushButton;
    QRadioButton *createStreamsradioButt;
    QRadioButton *DVBscanradioButton;
    QSpinBox *scan_adapter_spinBox;
    QLabel *label_13;
    QGroupBox *tunerAgroupBox;
    QSpinBox *channelAspinBox;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *tunerNameA;
    QLineEdit *tunerA_type;
    QLineEdit *tunerA_pol;
    QLineEdit *tunerA_SR;
    QLineEdit *FrequencyA;
    QGroupBox *tunerBgroupBox;
    QSpinBox *channelBspinBox;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *tunerNameB;
    QLineEdit *tunerB_type;
    QLineEdit *tunerB_pol;
    QLineEdit *tunerB_SR;
    QLineEdit *FrequencyB;
    QTextBrowser *textBrowser;
    QGroupBox *tunerCgroupBox;
    QSpinBox *channelCspinBox;
    QLabel *label_5;
    QLabel *label_6;
    QLineEdit *tunerNameC;
    QLineEdit *tunerC_type;
    QLineEdit *tunerC_pol;
    QLineEdit *tunerC_SR;
    QLineEdit *FrequencyC;
    QGroupBox *tunerDgroupBox;
    QSpinBox *channelDspinBox;
    QLabel *label_7;
    QLabel *label_8;
    QLineEdit *tunerNameD;
    QLineEdit *tunerD_type;
    QLineEdit *tunerD_pol;
    QLineEdit *tunerD_SR;
    QLineEdit *FrequencyD;
    QTextBrowser *textBrowser1;
    QGroupBox *streamGroupBox;
    QRadioButton *rtpButton;
    QRadioButton *udpButton;
    QLineEdit *IPlineEdit;
    QSpinBox *width_spinBox;
    QSpinBox *height_spinBox;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QDoubleSpinBox *ScaleSpinBox;
    QLabel *label_12;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *adtv)
    {
        if (adtv->objectName().isEmpty())
            adtv->setObjectName(QStringLiteral("adtv"));
        adtv->setWindowModality(Qt::NonModal);
        adtv->resize(867, 423);
        centralWidget = new QWidget(adtv);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        FunctiongroupBox = new QGroupBox(centralWidget);
        FunctiongroupBox->setObjectName(QStringLiteral("FunctiongroupBox"));
        FunctiongroupBox->setGeometry(QRect(10, 0, 151, 191));
        tuneTo_radioButt = new QRadioButton(FunctiongroupBox);
        tuneTo_radioButt->setObjectName(QStringLiteral("tuneTo_radioButt"));
        tuneTo_radioButt->setGeometry(QRect(0, 60, 105, 21));
        tuneTo_radioButt->setChecked(false);
        ATSCscanradioButt = new QRadioButton(FunctiongroupBox);
        ATSCscanradioButt->setObjectName(QStringLiteral("ATSCscanradioButt"));
        ATSCscanradioButt->setEnabled(true);
        ATSCscanradioButt->setGeometry(QRect(0, 80, 105, 21));
        ATSCscanradioButt->setChecked(false);
        showStreamradioButt = new QRadioButton(FunctiongroupBox);
        showStreamradioButt->setObjectName(QStringLiteral("showStreamradioButt"));
        showStreamradioButt->setGeometry(QRect(0, 40, 121, 21));
        pushButton = new QPushButton(FunctiongroupBox);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(0, 160, 111, 27));
        createStreamsradioButt = new QRadioButton(FunctiongroupBox);
        createStreamsradioButt->setObjectName(QStringLiteral("createStreamsradioButt"));
        createStreamsradioButt->setEnabled(true);
        createStreamsradioButt->setGeometry(QRect(0, 20, 121, 21));
        createStreamsradioButt->setChecked(true);
        DVBscanradioButton = new QRadioButton(FunctiongroupBox);
        DVBscanradioButton->setObjectName(QStringLiteral("DVBscanradioButton"));
        DVBscanradioButton->setGeometry(QRect(0, 100, 104, 20));
        scan_adapter_spinBox = new QSpinBox(FunctiongroupBox);
        scan_adapter_spinBox->setObjectName(QStringLiteral("scan_adapter_spinBox"));
        scan_adapter_spinBox->setGeometry(QRect(0, 130, 47, 24));
        scan_adapter_spinBox->setMaximum(3);
        label_13 = new QLabel(FunctiongroupBox);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(50, 130, 71, 21));
        tunerAgroupBox = new QGroupBox(centralWidget);
        tunerAgroupBox->setObjectName(QStringLiteral("tunerAgroupBox"));
        tunerAgroupBox->setEnabled(false);
        tunerAgroupBox->setGeometry(QRect(330, 0, 120, 171));
        channelAspinBox = new QSpinBox(tunerAgroupBox);
        channelAspinBox->setObjectName(QStringLiteral("channelAspinBox"));
        channelAspinBox->setGeometry(QRect(0, 20, 52, 25));
        QPalette palette;
        QBrush brush(QColor(170, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::HighlightedText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::HighlightedText, brush);
        channelAspinBox->setPalette(palette);
        QFont font;
        font.setKerning(true);
        channelAspinBox->setFont(font);
        channelAspinBox->setCursor(QCursor(Qt::ArrowCursor));
        channelAspinBox->setMouseTracking(true);
        channelAspinBox->setFocusPolicy(Qt::WheelFocus);
        channelAspinBox->setContextMenuPolicy(Qt::DefaultContextMenu);
        channelAspinBox->setAutoFillBackground(false);
        channelAspinBox->setInputMethodHints(Qt::ImhDigitsOnly);
        channelAspinBox->setWrapping(false);
        channelAspinBox->setReadOnly(false);
        channelAspinBox->setKeyboardTracking(true);
        channelAspinBox->setMinimum(0);
        channelAspinBox->setMaximum(51);
        channelAspinBox->setValue(0);
        label = new QLabel(tunerAgroupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(60, 20, 57, 15));
        label_2 = new QLabel(tunerAgroupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(80, 50, 57, 15));
        tunerNameA = new QLineEdit(tunerAgroupBox);
        tunerNameA->setObjectName(QStringLiteral("tunerNameA"));
        tunerNameA->setGeometry(QRect(0, 80, 113, 25));
        tunerA_type = new QLineEdit(tunerAgroupBox);
        tunerA_type->setObjectName(QStringLiteral("tunerA_type"));
        tunerA_type->setGeometry(QRect(0, 110, 61, 24));
        tunerA_pol = new QLineEdit(tunerAgroupBox);
        tunerA_pol->setObjectName(QStringLiteral("tunerA_pol"));
        tunerA_pol->setGeometry(QRect(70, 110, 41, 24));
        tunerA_SR = new QLineEdit(tunerAgroupBox);
        tunerA_SR->setObjectName(QStringLiteral("tunerA_SR"));
        tunerA_SR->setGeometry(QRect(0, 140, 113, 24));
        FrequencyA = new QLineEdit(tunerAgroupBox);
        FrequencyA->setObjectName(QStringLiteral("FrequencyA"));
        FrequencyA->setGeometry(QRect(0, 50, 71, 24));
        FrequencyA->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        tunerBgroupBox = new QGroupBox(centralWidget);
        tunerBgroupBox->setObjectName(QStringLiteral("tunerBgroupBox"));
        tunerBgroupBox->setEnabled(false);
        tunerBgroupBox->setGeometry(QRect(470, 0, 120, 171));
        channelBspinBox = new QSpinBox(tunerBgroupBox);
        channelBspinBox->setObjectName(QStringLiteral("channelBspinBox"));
        channelBspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelBspinBox->setMinimum(0);
        channelBspinBox->setMaximum(51);
        channelBspinBox->setValue(0);
        label_3 = new QLabel(tunerBgroupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(60, 20, 57, 15));
        label_4 = new QLabel(tunerBgroupBox);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(70, 50, 57, 15));
        tunerNameB = new QLineEdit(tunerBgroupBox);
        tunerNameB->setObjectName(QStringLiteral("tunerNameB"));
        tunerNameB->setGeometry(QRect(0, 80, 113, 25));
        tunerB_type = new QLineEdit(tunerBgroupBox);
        tunerB_type->setObjectName(QStringLiteral("tunerB_type"));
        tunerB_type->setGeometry(QRect(0, 110, 61, 24));
        tunerB_pol = new QLineEdit(tunerBgroupBox);
        tunerB_pol->setObjectName(QStringLiteral("tunerB_pol"));
        tunerB_pol->setGeometry(QRect(70, 110, 41, 24));
        tunerB_SR = new QLineEdit(tunerBgroupBox);
        tunerB_SR->setObjectName(QStringLiteral("tunerB_SR"));
        tunerB_SR->setGeometry(QRect(0, 140, 113, 24));
        FrequencyB = new QLineEdit(tunerBgroupBox);
        FrequencyB->setObjectName(QStringLiteral("FrequencyB"));
        FrequencyB->setGeometry(QRect(0, 50, 61, 24));
        FrequencyB->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        textBrowser = new QTextBrowser(centralWidget);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));
        textBrowser->setGeometry(QRect(170, 170, 691, 111));
        textBrowser->setFrameShadow(QFrame::Sunken);
        textBrowser->setLineWidth(3);
        tunerCgroupBox = new QGroupBox(centralWidget);
        tunerCgroupBox->setObjectName(QStringLiteral("tunerCgroupBox"));
        tunerCgroupBox->setEnabled(false);
        tunerCgroupBox->setGeometry(QRect(610, 0, 120, 171));
        channelCspinBox = new QSpinBox(tunerCgroupBox);
        channelCspinBox->setObjectName(QStringLiteral("channelCspinBox"));
        channelCspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelCspinBox->setMinimum(0);
        channelCspinBox->setMaximum(51);
        channelCspinBox->setValue(0);
        label_5 = new QLabel(tunerCgroupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(60, 20, 57, 15));
        label_6 = new QLabel(tunerCgroupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(70, 50, 57, 15));
        tunerNameC = new QLineEdit(tunerCgroupBox);
        tunerNameC->setObjectName(QStringLiteral("tunerNameC"));
        tunerNameC->setGeometry(QRect(0, 80, 113, 25));
        tunerC_type = new QLineEdit(tunerCgroupBox);
        tunerC_type->setObjectName(QStringLiteral("tunerC_type"));
        tunerC_type->setGeometry(QRect(0, 110, 61, 24));
        tunerC_pol = new QLineEdit(tunerCgroupBox);
        tunerC_pol->setObjectName(QStringLiteral("tunerC_pol"));
        tunerC_pol->setGeometry(QRect(70, 110, 41, 24));
        tunerC_SR = new QLineEdit(tunerCgroupBox);
        tunerC_SR->setObjectName(QStringLiteral("tunerC_SR"));
        tunerC_SR->setGeometry(QRect(0, 140, 113, 24));
        FrequencyC = new QLineEdit(tunerCgroupBox);
        FrequencyC->setObjectName(QStringLiteral("FrequencyC"));
        FrequencyC->setGeometry(QRect(0, 50, 61, 24));
        FrequencyC->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        tunerDgroupBox = new QGroupBox(centralWidget);
        tunerDgroupBox->setObjectName(QStringLiteral("tunerDgroupBox"));
        tunerDgroupBox->setEnabled(false);
        tunerDgroupBox->setGeometry(QRect(740, 0, 120, 171));
        channelDspinBox = new QSpinBox(tunerDgroupBox);
        channelDspinBox->setObjectName(QStringLiteral("channelDspinBox"));
        channelDspinBox->setGeometry(QRect(0, 20, 52, 25));
        channelDspinBox->setMinimum(0);
        channelDspinBox->setMaximum(51);
        channelDspinBox->setValue(0);
        label_7 = new QLabel(tunerDgroupBox);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(60, 20, 57, 15));
        label_8 = new QLabel(tunerDgroupBox);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(70, 50, 57, 15));
        tunerNameD = new QLineEdit(tunerDgroupBox);
        tunerNameD->setObjectName(QStringLiteral("tunerNameD"));
        tunerNameD->setGeometry(QRect(0, 80, 113, 25));
        tunerD_type = new QLineEdit(tunerDgroupBox);
        tunerD_type->setObjectName(QStringLiteral("tunerD_type"));
        tunerD_type->setGeometry(QRect(0, 110, 61, 24));
        tunerD_pol = new QLineEdit(tunerDgroupBox);
        tunerD_pol->setObjectName(QStringLiteral("tunerD_pol"));
        tunerD_pol->setGeometry(QRect(70, 110, 41, 24));
        tunerD_SR = new QLineEdit(tunerDgroupBox);
        tunerD_SR->setObjectName(QStringLiteral("tunerD_SR"));
        tunerD_SR->setGeometry(QRect(0, 140, 113, 24));
        FrequencyD = new QLineEdit(tunerDgroupBox);
        FrequencyD->setObjectName(QStringLiteral("FrequencyD"));
        FrequencyD->setGeometry(QRect(0, 50, 61, 24));
        FrequencyD->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        textBrowser1 = new QTextBrowser(centralWidget);
        textBrowser1->setObjectName(QStringLiteral("textBrowser1"));
        textBrowser1->setGeometry(QRect(170, 290, 691, 111));
        textBrowser1->setTabChangesFocus(true);
        streamGroupBox = new QGroupBox(centralWidget);
        streamGroupBox->setObjectName(QStringLiteral("streamGroupBox"));
        streamGroupBox->setEnabled(true);
        streamGroupBox->setGeometry(QRect(0, 200, 161, 201));
        rtpButton = new QRadioButton(streamGroupBox);
        rtpButton->setObjectName(QStringLiteral("rtpButton"));
        rtpButton->setGeometry(QRect(0, 20, 61, 21));
        rtpButton->setChecked(false);
        udpButton = new QRadioButton(streamGroupBox);
        udpButton->setObjectName(QStringLiteral("udpButton"));
        udpButton->setGeometry(QRect(0, 40, 51, 21));
        udpButton->setChecked(true);
        IPlineEdit = new QLineEdit(streamGroupBox);
        IPlineEdit->setObjectName(QStringLiteral("IPlineEdit"));
        IPlineEdit->setGeometry(QRect(10, 60, 131, 25));
        width_spinBox = new QSpinBox(streamGroupBox);
        width_spinBox->setObjectName(QStringLiteral("width_spinBox"));
        width_spinBox->setGeometry(QRect(10, 100, 47, 24));
        width_spinBox->setMaximum(640);
        width_spinBox->setSingleStep(10);
        width_spinBox->setValue(180);
        height_spinBox = new QSpinBox(streamGroupBox);
        height_spinBox->setObjectName(QStringLiteral("height_spinBox"));
        height_spinBox->setGeometry(QRect(70, 100, 47, 24));
        height_spinBox->setMaximum(480);
        height_spinBox->setSingleStep(10);
        height_spinBox->setValue(100);
        label_9 = new QLabel(streamGroupBox);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(10, 130, 41, 16));
        label_10 = new QLabel(streamGroupBox);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(60, 110, 16, 16));
        label_11 = new QLabel(streamGroupBox);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(70, 130, 41, 16));
        ScaleSpinBox = new QDoubleSpinBox(streamGroupBox);
        ScaleSpinBox->setObjectName(QStringLiteral("ScaleSpinBox"));
        ScaleSpinBox->setGeometry(QRect(10, 150, 66, 24));
        ScaleSpinBox->setMinimum(0.1);
        ScaleSpinBox->setMaximum(1);
        ScaleSpinBox->setSingleStep(0.05);
        ScaleSpinBox->setValue(0.3);
        label_12 = new QLabel(streamGroupBox);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(10, 180, 59, 14));
        adtv->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(adtv);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        adtv->setStatusBar(statusBar);
        QWidget::setTabOrder(pushButton, channelAspinBox);
        QWidget::setTabOrder(channelAspinBox, channelBspinBox);
        QWidget::setTabOrder(channelBspinBox, channelCspinBox);
        QWidget::setTabOrder(channelCspinBox, channelDspinBox);
        QWidget::setTabOrder(channelDspinBox, tuneTo_radioButt);
        QWidget::setTabOrder(tuneTo_radioButt, showStreamradioButt);
        QWidget::setTabOrder(showStreamradioButt, ATSCscanradioButt);
        QWidget::setTabOrder(ATSCscanradioButt, textBrowser);
        QWidget::setTabOrder(textBrowser, textBrowser);

        retranslateUi(adtv);

        QMetaObject::connectSlotsByName(adtv);
    } // setupUi

    void retranslateUi(QMainWindow *adtv)
    {
        adtv->setWindowTitle(QApplication::translate("adtv", "adtv", 0));
        FunctiongroupBox->setTitle(QApplication::translate("adtv", "Tuner Function", 0));
        tuneTo_radioButt->setText(QApplication::translate("adtv", "Tune To", 0));
        ATSCscanradioButt->setText(QApplication::translate("adtv", "ATSC Scan", 0));
        showStreamradioButt->setText(QApplication::translate("adtv", "Show Video", 0));
        pushButton->setText(QApplication::translate("adtv", "Tune", 0));
        createStreamsradioButt->setText(QApplication::translate("adtv", "Create Streams", 0));
        DVBscanradioButton->setText(QApplication::translate("adtv", "DVB Scan", 0));
        label_13->setText(QApplication::translate("adtv", "ScanAdap", 0));
        tunerAgroupBox->setTitle(QApplication::translate("adtv", "Adapter0", 0));
        label->setText(QApplication::translate("adtv", "Channel", 0));
        label_2->setText(QApplication::translate("adtv", "MHz", 0));
        tunerBgroupBox->setTitle(QApplication::translate("adtv", "Adapter1", 0));
        label_3->setText(QApplication::translate("adtv", "Channel", 0));
        label_4->setText(QApplication::translate("adtv", "MHz", 0));
        textBrowser->setDocumentTitle(QString());
        tunerCgroupBox->setTitle(QApplication::translate("adtv", "Adapter2", 0));
        label_5->setText(QApplication::translate("adtv", "Channel", 0));
        label_6->setText(QApplication::translate("adtv", "MHz", 0));
        tunerDgroupBox->setTitle(QApplication::translate("adtv", "Adapter3", 0));
        label_7->setText(QApplication::translate("adtv", "Channel", 0));
        label_8->setText(QApplication::translate("adtv", "MHz", 0));
        streamGroupBox->setTitle(QApplication::translate("adtv", "Stream Type", 0));
        rtpButton->setText(QApplication::translate("adtv", "RTP", 0));
        udpButton->setText(QApplication::translate("adtv", "UDP", 0));
        IPlineEdit->setText(QApplication::translate("adtv", "127.0.0.1", 0));
        label_9->setText(QApplication::translate("adtv", "Width", 0));
        label_10->setText(QApplication::translate("adtv", "x", 0));
        label_11->setText(QApplication::translate("adtv", "Height", 0));
        label_12->setText(QApplication::translate("adtv", "Scale", 0));
    } // retranslateUi

};

namespace Ui {
    class adtv: public Ui_adtv {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADTV_H
